import styled, { css } from "styled-components";


export const container =  styled.nav`
    height: 56px;
    width: 100%;
    overflow: hidden;
    padding: 0 16px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    background:#fff !important;
   
   


`

export const start = styled.div`
display: flex;
flex-direction: row;


`
export const iconMenu = styled.button`

    border: none;
    font-size:24px;
    background:#fff;
    @media only screen and (max-width: 768px) {
    
      
        margin-left:5px;
         
    }
    @media only screen and (min-width: 769px) {
        
        margin-left:20px;
        
         
    }


`

export const  logoIcon = styled.div`
    padding:18px 14px 17px 16px;
    width:120px;
  
    

`
export const locationLanguage = styled.span`
    position: absolute;
    top: 5px;
    font-size:10px;
   

`



export const center = styled.div`
display: flex;
flex-direction: row;
align-items: center;
flex: 0 1 728px;
min-width: 0px;


`

export const iconMrophone = styled.button`
background:#f9f9f9;
border-radius:100%;
padding:5px 10px;
font-size:20px;
margin-left:5px;
border: none;
cursor: pointer;
`



export const end = styled.div`
    align-items: center;
    align-items: flex-end;
    margin-left:30px;
    display:inherit;

`

export const  iconEnd = styled.button`
font-size:20px;
border: none;
cursor: pointer;
background:#fff;
@media only screen and (max-width: 768px) {
    
    #Fauser{
        margin-right:0px;
        margin-left:20px;
    }
    
     
}
@media only screen and (min-width: 769px) {
    
    margin-right:20px;
    
     
}
`