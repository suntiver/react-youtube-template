import React ,{useEffect,useState} from 'react'
import Search from '../Search'
import * as S from './navbar.styled'
import {IoMenuSharp , IoAppsOutline , IoNotificationsOutline} from 'react-icons/io5'
import { MdMic  } from "react-icons/md";
import { VscDeviceCameraVideo } from "react-icons/vsc";
import { FaUserCircle } from "react-icons/fa";
import LogoYoutube from './LogoYoutube'

export default function Navbar({

  onClick

}) {

  const [inputSearch, setinputSearch] = useState("");

  const [matches, setmatches] = useState(window.matchMedia("(min-width: 768px)").matches);

  const [openMenu, setopenMenu] = useState(false);
  
  useEffect(() => {
    window
    .matchMedia("(min-width: 768px)")
    .addEventListener('change', e => setmatches( e.matches ));
  }, []);


  const onClickMenu = () =>{
    setopenMenu(!openMenu)
    onClick(openMenu)
  }

  return (
    <React.Fragment>
      <S.container>
        <S.start>
              <S.iconMenu onClick={onClickMenu}>  <IoMenuSharp   />  </S.iconMenu>
             
              <S.logoIcon>  <LogoYoutube /> <S.locationLanguage>TH </S.locationLanguage> </S.logoIcon>

        </S.start>
        {matches && <> 
          <S.center>
            <Search  placeholder="ค้นหา"  value={inputSearch}  onChange={(e)=>{setinputSearch(e.target.value)}}/>
            <S.iconMrophone onClick={()=>{}}>   <MdMic  />  </S.iconMrophone>
          </S.center>
          </>
        }
        <S.end>
          {matches && <>        
              <S.iconEnd onClick={()=>{}}>   <VscDeviceCameraVideo     />  </S.iconEnd>
              <S.iconEnd onClick={()=>{}}>   <IoAppsOutline   />  </S.iconEnd>
              <S.iconEnd onClick={()=>{}}>   <IoNotificationsOutline   />  </S.iconEnd> 
        </>
        }
        {!matches && <>
          <S.iconEnd onClick={()=>{}}>    <MdMic  />  </S.iconEnd>

        </>     
        }
         <S.iconEnd onClick={()=>{}}>   <FaUserCircle   id="Fauser" />  </S.iconEnd>

        </S.end>
      </S.container>
    </React.Fragment>
  )
}
