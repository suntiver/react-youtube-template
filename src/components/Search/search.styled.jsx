import styled, { css } from "styled-components";

export const searchbox = styled.div`
margin: 0 0 0 40px;
padding: 0 4px;
display: flex;
flex-derection: row;
flex:1;



`


export const container = styled.div`
  display: flex;
  flex-derection: row;
  position: relative;
  border: 1px solid #ccc;
  color: #333;
  padding: 2px 6px;
  box-shodow: inset 0 1px 2px #eee;
  flex: 1;
  flex-basis: 0.000000001px;

  ${(props) =>
    props.focused
      && css`
            border: 1px solid #1c62b9;
                box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
      `}
      
`;

export const searchIcon = styled.div`
  ${(props) =>
    !props.focused
      ? css`
          display: none;
        `
      : css`
          width: 20px;
          height: 20px;
        `}
        
  color:#030303;
  padding: 0 5px;
  margin-right: 20px;
  margin-top:5px;
`;

export const searchInput = styled.input`
  background-color: transparent;
  border: none;
  box-shadow: none;
  color: inherit;
  font-size: 1rem;
  font-weight: 400;
  line-height: 1rem;
  margin-left: 4px;
  max-width: 100%;
  outline: none;
  width: 100%;
  flex: 1;
  cursor: text;

`;


export const searchClearButton  = styled.button`
display:none;


${(props) => 
            props.search && css`
                height: 24px;
                width: 24px;
                display: flex;
                align-items: center;
                justify-content: center;
                margin-top:5px;
                background: none;
                color: inherit;
                border: none;
                cursor: pointer;
                font-size:1.3rem;
                              
            `
    
        }

`

export const searchIconLegacy = styled.button`
border: 1px solid #d3d3d3;
background-color: #f8f8f8;
border-radius: 0 2px 2px 0;
cursor: pointer;
height: 40px;
width: 64px;
margin: 0;
  
  &:hover{
    background-color:#e9e9e9;;
  }



`