import React , {useState , useEffect} from 'react'
import * as S from "./search.styled";
import {IoSearchOutline , IoCloseOutline} from 'react-icons/io5'
import PropTypes from 'prop-types'

export default function Search({
  type,
  placeholder,
  id,
  value,
  onChange

}) {

  const [focus, setfocused] = useState(false);

  const [search, setsearch] = useState("");

  useEffect(() => {
    setsearch(value)
  }, [value])


  const handleChange = (e) =>{

    setsearch(e.target.value)
    onChange(e)

  }

  const clearSearch = () =>{
    console.log("sssss");
    setsearch("")
  }
  return (
    <React.Fragment>
      <S.searchbox id-datatest={id}> 
       <S.container focused={focus}>
            <S.searchIcon focused={focus}><IoSearchOutline style={{fontSize:'1.3rem'}}  /></S.searchIcon>
            <S.searchInput type={type} placeholder={placeholder} value={search}  onChange={handleChange}  onFocus={()=>setfocused(true)}  onBlur={()=>setfocused(false)} />
            <S.searchClearButton search={search} onFocus={clearSearch} > <IoCloseOutline />  </S.searchClearButton>
       </S.container>
      
        <S.searchIconLegacy> <IoSearchOutline  style={{fontSize:'1.3rem'}} /> </S.searchIconLegacy>
        
       </S.searchbox>
    </React.Fragment>
  )
}


Search.propTypes = {
  type:PropTypes.string,
  onChange: PropTypes.func,
  onClick: PropTypes.func,
  id: PropTypes.string,
  placeholder:PropTypes.string

}

Search.defaultProps = {
  type: 'text',
  onChange: () => {},
  id: '',
  value: null,
  placeholder:''
}