import React,{useState , useEffect} from 'react'
import * as S from './home.styled'
import Navbar from "../Navbar";
import NavbarLeft from  '../NavbarLeft'
export default function Home() {

  const [clickMenu, setClickMenu] = useState(window.matchMedia("(min-width: 768px)").matches);

  useEffect(() => {
    window
    .matchMedia("(min-width: 768px)")
    .addEventListener('change', e => setClickMenu(e.matches));
  }, []);

  return (
        <React.Fragment> 
            <S.GlobalStyle />
            <S.Container>
              <Navbar onClick={(e)=>{setClickMenu(e)}}/>
              
              <S.Conten>

                   <NavbarLeft actived={clickMenu} />
                   <div>
                      <h1>List video </h1>
                   </div>


              </S.Conten>
        
             </S.Container>



        </React.Fragment>
       

        
  )
}
