import React , {useState} from "react";
import * as S from "./navbarleft.styled";
import { IoHomeOutline,IoHomeSharp,IoCompassOutline,IoCompass ,IoAccessibilityOutline, IoAccessibility  , IoFileTrayOutline,
  IoFileTraySharp , IoLogoYoutube , IoMusicalNotesOutline,IoMusicalNotes ,
   IoFolderOpenOutline , IoFolderOpenSharp , IoTimeOutline,IoTime ,IoPlayCircleOutline,IoPodiumOutline,IoPodiumSharp ,
   IoTimerSharp , IoArrowDownOutline , IoThumbsUpOutline,IoThumbsUpSharp} from "react-icons/io5";

export default function NavbarLeft({actived}) {


  const [clickMenu, setClickmenu] = useState(1);
   
 const listMenu_1 = [
  {
    id:1,
    menu_th: "หน้าเเรก"

  },
  { 
    id:2,
    menu_th: "สำรวจ"
  },
  {
    id:3,
    menu_th: "Shorts"
  },
  {
    id:4,
    menu_th: "การติดตาม"
  },
  {
    id:5,
    menu_th: "Originals"
  },
  {
    id:6,
    menu_th: "Youtube Music"
  },
  {
    id:7,
    menu_th: "คลังวิดีโอ"

  },
  { 
    id:8,
    menu_th: "ประวัติการเข้าชม"
  },
  {
    id:9,
    menu_th: "วีดีโอของคุณ"
  },
  {
    id:10,
    menu_th: "ดูภายหลัง"
  },
  {
    id:11,
    menu_th: "การดาวน์โหลด"
  },
  {
    id:12,
    menu_th: "วีดีโอที่ชอบ"
  },
  { 
    id:13,
    menu_th: "ประวัติการเข้าชม"
  },
  {
    id:14,
    menu_th: "วีดีโอของคุณ"
  },
  {
    id:15,
    menu_th: "ดูภายหลัง"
  },
  {
    id:16,
    menu_th: "การดาวน์โหลด"
  },
  {
    id:17,
    menu_th: "วีดีโอที่ชอบ"
  },
]

const rederIcon = (data) => {
  if(data === 1){
      //  return(<IoHomeSharp id="icon" />)
      if(clickMenu === data){
        return(<IoHomeSharp id="icon" />)
      }
      return(<IoHomeOutline id="icon" />)
  }else if(data == 2){
    if(clickMenu === data){
      return(<IoCompass id="icon" />)
    }

    return(<IoCompassOutline id="icon" />)
  }else if(data == 3){
    if(clickMenu === data){
       return(<IoAccessibility id="icon" />)
    }
    return(<IoAccessibilityOutline id="icon" />)
  }else if(data == 4){
    if(clickMenu === data){
       return(<IoFileTraySharp id="icon" />)
    }
    return(<IoFileTrayOutline id="icon" />)
  }else if(data == 5){
    
      return(<IoLogoYoutube id="icon" />)
  }else if(data == 6){
    if(clickMenu === data){
       return(<IoMusicalNotes id="icon" />)
    }  
    return(<IoMusicalNotesOutline id="icon" />)
  }else if(data == 7){
    if(clickMenu === data){
       return(<IoFolderOpenSharp id="icon" />)
    }
    return(<IoFolderOpenOutline id="icon" />)
  }else if(data == 8){
    if(clickMenu === data){
        return(<IoTime id="icon" />)
    }
    return(<IoTimeOutline id="icon" />)
  }else if(data == 9){
    if(clickMenu === data){
       return(<IoPodiumSharp id="icon" />)
    }
    return(<IoPodiumOutline id="icon" />)
  }else if(data == 10){
    if(clickMenu === data){
        return(<IoTimerSharp id="icon" />)
    }
    return(<IoPlayCircleOutline id="icon" />)
  }else if(data == 11){
    if(clickMenu === data){
      return(<IoArrowDownOutline id="icon" />)
    }
    return(<IoArrowDownOutline id="icon" />)
  }else if(data == 12){
    if(clickMenu === data){
      return(<IoThumbsUpSharp id="icon" />)
    }
    return(<IoThumbsUpOutline id="icon" />)
  }
}


  return (
    <React.Fragment>
      {actived && (

<S.container>
        {listMenu_1.map((data, index) => (
          data.id === 7 || data.id === 14  ? <><S.collapsible/>
          <S.Ul key={index}>
          <S.li>
            <S.tagA  actived={clickMenu === data.id ? true : false} onClick={()=>{setClickmenu(data.id)}}>
            {rederIcon(data.id)}
              
              <S.textMenuLeft>{data.menu_th}</S.textMenuLeft>
          
            </S.tagA>
          </S.li>
        </S.Ul>
        </> :
          <S.Ul key={index}>
            <S.li>
              <S.tagA  actived={clickMenu === data.id ? true : false} onClick={()=>{setClickmenu(data.id)}}   >
           
                {rederIcon(data.id)}
                <S.textMenuLeft>{data.menu_th}</S.textMenuLeft>
            
              </S.tagA>
            </S.li>
          </S.Ul>
        ))}
  
      </S.container>
      )}
      
    </React.Fragment>
  );
}
