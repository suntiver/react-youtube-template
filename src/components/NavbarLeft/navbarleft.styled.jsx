import styled , { css } from 'styled-components';

export const container = styled.section`
transition: 0.5s;
margin-bottom:100px;
background:#fff;
position:fixed;
left:0;
width:250px;
height:100%;
padding-top:10px;
padding-bottom: 80px;
overflow-y: auto;
::-webkit-scrollbar {
    width: 16px;
}

::-webkit-scrollbar-track {
    border-radius: 8px;
}

::-webkit-scrollbar-thumb {
    height: 56px;
    border-radius: 8px;
    border: 4px solid transparent;
    background-clip: content-box;
    background-color: #888;
}

::-webkit-scrollbar-thumb:hover {
    background-color: #555;
}

`

export const Ul = styled.ul`


`

export const li = styled.li`
`

export const textMenuLeft = styled.label`
    font-family: "Roboto","Arial",sans-serif;
    font-size: 16px;
    line-height: 2rem;
    font-weight: 500;

`

export const tagA = styled.a`
 display:block;
 height:100%;
 width:98%;
 line-height:2.6rem;
 font-size:20px;
 padding-left:30px;
 ${(props) => 
    props.actived && css`
        background:#e5e5e5;
                      
    `

}

 :hover{
    ${(props) => !props.actived ? 'background:#f2f2f2' : ''}
 }

 #icon{
    margin-right:24px;
 }

`

export const  collapsible  = styled.hr`
border-top: 1px solid #0000001a;
margin: 16px 0;


`

export const IconMenu = styled.div`



`